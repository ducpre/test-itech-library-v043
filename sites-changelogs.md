#Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
- Update font-size of text in table, height of row, navbar-css, MyfilterBox - [@TinVan2]
- Update layout - [@TinVan2]
### Added
- Support check checkbox action in list - [@TinVan2]
## [0.0.22] - 2021-02-19
### Changed
- Update auto hide notification for crud action - [@TinVan2]
- Update onClick event to sub menu - [@TinVan2]
## [0.0.21] - 2021-02-06
### Added
- Turn off auto sidebar when user click toggle - [@TinVan2]
- Update MyDatePicking - [@TinVan2]
- Add customize My List background color - [@Triet]
## [0.0.20] - 2021-02-06
### Changed
- Update text translate - [@TinVan2]
- Update size of button search in list - [@TinVan2]
## [0.0.19] - 2021-02-03
### Changed
- Increase display time of notification - [@TinVan2]
- Support close notification - [@TinVan2]
## [0.0.18] - 2021-02-02
### Added
- Add message for response code 401 Unauthorized - [@Triet]
## [0.0.17] - 2021-02-01
### Added
- Add translate text for DatePicking - [@TinVan2]
- Add close when unmount in messagebox - [@TinVan2]
- Support hide header of table - [@TinVan2]
## [0.0.16] - 2021-01-29
### Added
- Update message box for auto hide notification - [@TinVan2]
- Export logoString - [@Triet]
## [0.0.15] - 2021-01-25
### Added
- Added iTech favicon svg logo - [@Triet]

## [0.0.14] - 2021-01-21
### Added
- Added default logo definitions for themes - [@Triet]
## [0.0.13] - 2021-01-20
### Changed
- Change position of close button and action button in MessageBox - [@TinVan2]
## [0.0.12] - 2021-01-18
### Added
- Added multiple sort - [@TinVan2]
## [0.0.11] - 2021-01-10
### Fixed
- Fix warning when use div in MySaveToolbar and MyEditTOolbar - [@TinVan2]
## [0.0.10] - 2021-01-08
### Fixed
- Fix display save button with edit button - [@TinVan2]
## [0.0.9] - 2021-01-08
### Changed
- Change position of back button to right side of page - [@TinVan2]
## [0.0.8] - 2020-12-31
### Changed
- Support css of languageSwitcher, themeswitcher same vietrad - [@TinVan2]
## [0.0.7] - 2020-12-23
### Fixed
- Fix Itech theme light - [@Triet]
- Add sidebar header css definition - [@Triet]

## [0.0.6] - 2020-12-18
### Fixed
- Add form-inline to radio buttons groups - [@Triet]

## [0.0.5] - 2020-12-18
### Fixed
- Reduce size of radio buttons and fix radio buttons misalignment - [@Triet]

## [0.0.4] - 2020-12-18
### Changed
- Change theme color and table active row - [@Triet]
## [0.0.3] - 2020-12-17
### Added
- Support split pagination - [@TinVan2]
## [0.0.2] - 2020-12-11
### Added
- Added contrast color for each color variable - [@Triet]
### Changed
- Change theme color according to mockup - [@Triet]
- Change name of revert edit button - [@TinVan2]